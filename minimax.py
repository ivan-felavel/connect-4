# conding: utf8
"""
Uso la biblioteca copy para por valor la matriz con
el tablero en las llamadas recursivas.
"""
from copy import deepcopy
from utils import get_line_score, utility, is_a_valid_action, result, is_full

INF = 1000000000
EMPTY = ' '
MAX_DEPTH = 3

def min_value(board, depth):
    """
    Calcula la utilidad en el nivel del jugador de minimización.
    :param board: el estado del tablero
    :type board: list 
    :param depth: la profundidad actual del árbol de juego.
    :type depth: int
    """
    board_val = utility(board)
    if board_val == INF or board_val == -INF or is_full(board) or depth == MAX_DEPTH:
        return board_val
    value = INF
    actions = [0, 1, 2, 3, 4, 5, 6]
    valid_actions = [a for a in actions if is_a_valid_action(board, a)]
    for action in valid_actions:
        value = min(value, max_value(result(deepcopy(board), action, 'o'), depth + 1))
    return value

def max_value(board, depth):
    """
    Calcula la utilidad en el nivel del jugador de maximización.
    :param board: el estado del tablero
    :type board: list 
    :param depth: la profundidad actual del árbol de juego.
    :type depth: int
    """
    board_val = utility(board)
    if board_val == INF or board_val == -INF or is_full(board) or depth == MAX_DEPTH:
        # print("LLego a nodo terminal max value depth = {}".format(depth))
        # print_board(board)
        return board_val
    value = -INF
    actions = [0, 1, 2, 3, 4, 5, 6]
    valid_actions = [a for a in actions if is_a_valid_action(board, a)]
    for action in valid_actions:
        value = max(value, min_value(result(deepcopy(board), action, 'x'), depth + 1))
    return value

def minimax_decision(board, max_depth):
    """
    Retorna la decisión con el máximo valor después de aplicar recursivamente
    min_value y max_value.
    """
    global MAX_DEPTH
    MAX_DEPTH = max_depth
    actions = [0, 1, 2, 3, 4, 5, 6]
    valid_actions = [a for a in actions if is_a_valid_action(board, a)]
    depth = 0
    action_value = 0
    max_action_value = -10 * INF
    max_action = None
    for a in valid_actions:
        action_value = min_value(result(deepcopy(board), a, 'x'), depth)
        if action_value > max_action_value:
            max_action = a
            max_action_value = action_value
            if action_value >= INF:
                print("Gano si me muevo a la columna {}". format(max_action))
    return max_action
