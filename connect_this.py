# encoding: utf-8
import random
import pygame
from pygame.locals import *
import button
import minimax
import alpha_beta

SCREEN_SIZE = (800, 480)
SCREEN_WIDTH = SCREEN_SIZE[0]
SCREEN_HEIGHT = SCREEN_SIZE[1]

class ConnectThis(object):
    def __init__(self):
        self.done = False
        self.window_size = (SCREEN_WIDTH, SCREEN_HEIGHT)
        self.screen = pygame.display.get_surface()
        self.clock = pygame.time.Clock()
        self.fps = 60
        self.background = pygame.image.load('./static/background.png')
        self.background = pygame.transform.scale(self.background, SCREEN_SIZE)
        self.red_disc = pygame.image.load('./static/red.png')
        self.red_disc = pygame.transform.scale(self.red_disc, [70, 70])
        self.black_disc = pygame.image.load('./static/black.png')
        self.black_disc = pygame.transform.scale(self.black_disc, [70, 70])
        self.tile = pygame.image.load('./static/tile.png')
        self.tile = pygame.transform.scale(self.tile, [70, 70])
        self.ia_thinking = pygame.image.load('./static/ia_thinking.png')
        self.init_menu = True
        self.board = [[' ' for i in range(7)] for j in range(6)]
        self.ia_won = False
        self.human_won = False
        self.someone_won = False
        self.button_human = pygame.image.load('./static/button_human.png')
        self.button_ia = pygame.image.load('./static/button_ia.png')
        self.button_tie = pygame.image.load('./static/button_empate-reintentar.png')
        self.ia_turn = False
        self.show_easy_button = True
        self.show_med_button = True
        self.show_hard_button = True
        self.show_minimax_button = True
        self.show_alphabeta_button = True
        self.max_depth = 1
        self.algorithm = None
        self.difficulty_selected = False
        self.algorithm_selected = False
    def event_loop(self):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and not self.init_menu and not self.someone_won and not self.ia_turn:
                pos = pygame.mouse.get_pos()
                if 160 <= pos[0] <= 640:
                    action = (pos[0] - 160) // 70
                    if not self.is_valid(action):
                        continue
                    self.update_board(action, 'o')
                    board_utility = minimax.utility(self.board)
                    print("Board utility {}".format(board_utility))
                    if board_utility == -minimax.INF:
                        self.human_won = True
                        self.someone_won = True
                    else:
                        self.ia_turn = True
            if event.type == pygame.QUIT:
                self.done = True
            
    def is_valid(self, action):
        return self.board[0][action] == ' '
    def is_board_full(self):
        for line in self.board:
            if ' ' in line:
                return False
        return True
    def update_board(self, action, symbol):
        if self.is_valid(action):
            row_idx = None
            for row in range(len(self.board)):
                if self.board[row][action] == ' ':
                    row_idx = row
                else:
                    break
            if row_idx != None:
                self.board[row_idx][action] = symbol
    def reset(self):
        self.board = [[' ' for i in range(7)] for j in range(6)]
        self.ia_won = False
        self.human_won = False
        self.someone_won = False
        self.init_menu = True
        self.show_easy_button = True
        self.show_med_button = True
        self.show_hard_button = True
        self.show_minimax_button = True
        self.show_alphabeta_button = True
        self.algorithm = None
        self.max_depth = 1
        self.difficulty_selected = False
        self.algorithm_selected = False
    def run(self):
        again_human_button = button.Button(
            "", SCREEN_WIDTH // 2 - (self.button_human.get_size()[0]) // 2, SCREEN_HEIGHT // 2 - (self.button_human.get_size()[1] // 2), self.button_human.get_size()[0], self.button_human.get_size()[1], "./static/button_human.png")
        again_ai_button = button.Button(
            "", SCREEN_WIDTH // 2 - (self.button_ia.get_size()[0]) // 2, SCREEN_HEIGHT // 2 - (self.button_ia.get_size()[1] // 2), self.button_ia.get_size()[0], self.button_ia.get_size()[1], "./static/button_ia.png")
        tie_button = button.Button("", SCREEN_WIDTH // 2 - (self.button_tie.get_size()[0]) // 2, SCREEN_HEIGHT // 2 - (self.button_tie.get_size()[1] // 2), self.button_tie.get_size()[0], self.button_tie.get_size()[1], "./static/button_empate-reintentar.png")
        easy_button = button.Button("", 200, 136, 120, 56, "./static/button_facil.png")
        med_button = button.Button("", 340, 136, 120, 56, "./static/button_medio.png")
        hard_button = button.Button("", 480, 136, 120, 56, "./static/button_dificil.png")
        minimax_button = button.Button("", 202, 212, 145, 56, "./static/button_minimax.png")
        alphabeta_button = button.Button("", 367, 212, 231, 56, "./static/button_poda-alfa-beta.png")
        start_button = button.Button("", 323, 288, 154, 56, "./static/button_empezar.png")
        while not self.done:
            self.screen.fill((55, 60, 68))
            self.event_loop()
            self.screen.blit(self.background, [0, 0])
            for i in range(6):
                for j in range(7):
                    self.screen.blit(self.tile, [160 + j * 70 , 24 + i * 70])
                    if self.board[i][j] == 'x':
                        self.screen.blit(self.red_disc, [160 + j * 70 , 24 + i * 70])
                    if self.board[i][j] == 'o':
                        self.screen.blit(self.black_disc, [160 + j * 70 , 24 + i * 70])
            if self.init_menu:
                if self.show_easy_button:
                    easy_button.draw(self.screen)
                if self.show_med_button:
                    med_button.draw(self.screen)
                if self.show_hard_button:
                    hard_button.draw(self.screen)
                if self.show_minimax_button:
                    minimax_button.draw(self.screen)
                if self.show_alphabeta_button:
                    alphabeta_button.draw(self.screen)
                start_button.draw(self.screen)
                if easy_button.click():
                    self.difficulty_selected = True
                    self.max_depth = 2
                    self.show_hard_button = self.show_med_button = False
                if med_button.click():
                    self.difficulty_selected = True
                    self.max_depth = 3
                    self.show_easy_button = self.show_hard_button = False
                if hard_button.click():
                    self.difficulty_selected = True
                    self.max_depth = 4
                    self.show_easy_button = self.show_med_button = False
                if minimax_button.click():
                    self.algorithm_selected = True
                    self.algorithm = minimax.minimax_decision
                    self.show_alphabeta_button = False
                if alphabeta_button.click():
                    self.algorithm_selected = True
                    self.algorithm = alpha_beta.alpha_beta_search
                    self.show_minimax_button = False
                if start_button.click() and self.difficulty_selected and self.algorithm_selected:
                    self.init_menu = False
                    self.ia_turn = True if random.randint(1, 10) > 5 else False
            if self.is_board_full():
                tie_button.draw(self.screen)
            if self.human_won:
                again_human_button.draw(self.screen)
            if self.ia_won:
                again_ai_button.draw(self.screen)
            if again_ai_button.click() or again_human_button.click() or tie_button.click():
                self.reset()
            if self.ia_turn:
                self.screen.blit(self.ia_thinking, [SCREEN_WIDTH - self.ia_thinking.get_size()[0], 0])
            pygame.display.update()
            self.clock.tick(self.fps)
            if self.ia_turn:
                machine_action = self.algorithm(self.board, self.max_depth)
                # print("La máquina se mueve a {}:".format(machine_action))
                self.update_board(machine_action, 'x')
                board_utility = minimax.utility(self.board)
                print("Board utility {}".format(board_utility))
                if board_utility == minimax.INF:
                    self.ia_won = True
                    self.someone_won = True
                self.ia_turn = False

def main():
    pygame.init()
    pygame.display.set_caption("Connect this")
    pygame.display.set_mode(SCREEN_SIZE)
    ConnectThis().run()
    pygame.quit()

if __name__ == "__main__":
    main()
