# coding: utf-8

DIAGONALS = [[(2, 0), (3, 1), (4, 2), (5, 3)], [(1, 0), (2, 1), (3, 2), (4, 3)], [(2, 1), (3, 2), (4, 3), (5, 4)], [(0, 0), (1, 1), (2, 2), (3, 3)], [(1, 1), (2, 2), (3, 3), (4, 4)], [(2, 2), (3, 3), (4, 4), (5, 5)], [(0, 1), (1, 2), (2, 3), (3, 4)], [(1, 2), (2, 3), (3, 4), (4, 5)], [(2, 3), (3, 4), (4, 5), (5, 6)], [(0, 2), (1, 3), (2, 4), (3, 5)], [(1, 3), (2, 4), (3, 5), (4, 6)], [(0, 3), (1, 4), (2, 5), (3, 6)],
             [(3, 0), (2, 1), (1, 2), (0, 3)], [(4, 0), (3, 1), (2, 2), (1, 3)], [(3, 1), (2, 2), (1, 3), (0, 4)], [(5, 0), (4, 1), (3, 2), (2, 3)], [(4, 1), (3, 2), (2, 3), (1, 4)], [(3, 2), (2, 3), (1, 4), (0, 5)], [(5, 1), (4, 2), (3, 3), (2, 4)], [(4, 2), (3, 3), (2, 4), (1, 5)], [(3, 3), (2, 4), (1, 5), (0, 6)], [(5, 2), (4, 3), (3, 4), (2, 5)], [(4, 3), (3, 4), (2, 5), (1, 6)], [(5, 3), (4, 4), (3, 5), (2, 6)]]

INF = 1000000000
EMPTY = ' '

def get_line_score(line, player_symbol='x', enemy_symbol='o', empty_symbol=' '):
    """
    Función para obtener el puntaje una línea de tamaño cuatro.
    
    - Si tiene 4 'x' regreso INF
    - Si tiene 4 'o' regreso -INF
    - Si no hay fichas o hay fichas de los dos regreso 0
    - Si sólo fichas 'o' regreso un puntaje negativo de 10^(# fichas) 'o'
    - Si sólo fichas 'x' regreso un puntaje positivo de 10^(# fichas) 'x'
    
    :param line: una lista de tamaño cuatro con 'x's y 'o's.
    :type line: list
    :return: puntaje de esa línea
    :return type: int
    """
    if line == [player_symbol] * 4:
        return INF
    if line == [enemy_symbol] * 4:
        return -INF
    if line == [empty_symbol] * 4:
        return 0
    enemy_tokens = sum([1 for i in line if i == enemy_symbol])
    player_tokens = sum([1 for i in line if i == player_symbol])
    if enemy_tokens > 0 and player_tokens > 0:
        return 0
    if enemy_tokens > 0 and player_tokens == 0:
        return -(20 ** enemy_tokens)
    return 10 ** player_tokens

def utility(board):
    """
    Retorna el valor de la función de utilidad para un
    tablero dado.

    :param board: La matriz que con 'x' y 'o's.
    :type board: list
    :return: la utilidad del estado del tablero
    :return type: int.
    """
    line_size = 4
    rows = len(board)
    columns = len(board[0])
    counter = 0
    score = 0
    # Verticales
    for column in range(columns):
        for i in range(rows - line_size + 1):
            line = [board[j][column] for j in range(i, i + line_size)]
            line_score = get_line_score(line)
            if line_score == INF:
                return INF
            if line_score == -INF:
                return -INF
            score += line_score
    # Horizontales
    for row in range(rows):        
        for i in range(columns - line_size + 1):
            line = [board[row][j] for j in range(i, i + line_size)]
            score = get_line_score(line)
            line_score = get_line_score(line)
            if line_score == INF:
                return INF
            if line_score == -INF:
                return -INF
            score += line_score
    # Diagonales
    for diagonal in DIAGONALS:
        line = [board[l[0]][l[1]] for l in diagonal]
        score += get_line_score(line)
        line_score = get_line_score(line)
        if line_score == INF:
            return INF
        if line_score == -INF:
            return -INF
        score += line_score
    return score

def is_a_valid_action(board, action):
    """
    Verifica si puedo poner una ficha sobre una columna
    comprobando que hay aún hay espacios vacíos en esa columna.
    """
    return board[0][action] == EMPTY

def result(board, action, symbol):
    """
    Retorna un nuevo tablero después de haber puesto
    una ficha con el valor symbol en la columna dada por action.

    :param board: el tablero
    :type board: list
    :param action: un valor entre [0, 6], la columna en la que hay que poner la ficha
    :type action: int
    :param symbol: la ficha que se va tirar en el tablero 'x' o 'o'
    :type symbol: str
    :return: un nuevo tablero
    :return type: list
    """
    row_idx = None
    for row in range(len(board)):
        if board[row][action] == EMPTY:
            row_idx = row
        else:
            break
    if row_idx != None:
        board[row_idx][action] = symbol
    return board


def is_full(board):
    """
    Checa si el tablero está lleno.
    """
    for row in board:
        if EMPTY in row:
            return False
    return True
