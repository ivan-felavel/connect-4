# conding: utf8
import random
from copy import deepcopy
from utils import get_line_score, utility, is_a_valid_action, result, is_full

INF = 1000000000
EMPTY = ' '
MAX_DEPTH = 3


def min_value(board, depth, alpha, beta):
    board_val = utility(board)
    if board_val == INF or board_val == -INF or is_full(board) or depth == MAX_DEPTH:
        return board_val
    value = INF
    actions = [0, 1, 2, 3, 4, 5, 6]
    valid_actions = [a for a in actions if is_a_valid_action(board, a)]
    for action in valid_actions:
        value = min(value, max_value(
            result(deepcopy(board), action, 'o'), depth + 1, alpha, beta))
        if value <= alpha:
            return value
        beta = min(beta, value)
    return value

def max_value(board, depth, alpha, beta):
    board_val = utility(board)
    if board_val == INF or board_val == -INF or is_full(board) or depth == MAX_DEPTH:
        return board_val
    value = -INF
    actions = [0, 1, 2, 3, 4, 5, 6]
    valid_actions = [a for a in actions if is_a_valid_action(board, a)]
    for action in valid_actions:
        value = max(value, min_value(
            result(deepcopy(board), action, 'x'), depth + 1, alpha, beta))
        if value >= beta:
            return value
        alpha = max(alpha, value)
    return value

def alpha_beta_search(board, max_depth):
    global MAX_DEPTH
    MAX_DEPTH = max_depth
    actions = [0, 1, 2, 3, 4, 5, 6]
    valid_actions = [a for a in actions if is_a_valid_action(board, a)]
    depth = 0
    alpha = -2 * INF
    beta = INF
    max_action = None
    for a in valid_actions:
        value = min_value(result(deepcopy(board), a, 'x'), depth, alpha, beta)
        if value > alpha:
            alpha = value
            max_action = a
    return max_action
