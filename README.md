# Conecta 4

 Es un juego de mesa para dos jugadores en el que se introducen fichas en un tablero vertical con el objetivo de alinear cuatro consecutivas de un mismo color.

 En este proyecto se creó una versión
 del juego  en el que una máquina juega contra un humano y ésta intenta vencer utilizando los algoritmos minimax y alfa-beta.

![](2018-11-13-17-37-51.png)
![](2018-11-13-17-39-02.png)

## Función de utilidad

La función de utilidad, *utility(board)*, para evaluar el 
estado de un tablero se calcula obteniendo
cada una de las posibles líneas de tamaño
4 en todo el tablero y para cada una de éstas se asignan los 
siguientes puntajes:

- Si hay cuatro fichas de la máquina entonces el valor del tablero es INFINITO.
- Si hay cuatro fichas del enemigo
entonces el valor del tablero es -INFINITO.
- Si hay cero o hay fichas de ambos jugadores entonces el valor del tablero es CERO.
- Si hay 1, 2 o 3 fichas de la máquina entonces el valor del tablero es 10¹, 10² o 10³, respectivamente.
- Si hay 1, 2 o 3 fichas del enemigo entonces el valor del tablero es -20¹, -20² o 20³, respectivamente.

Por ejemplo, la función para el siguiente tablero tiene el siguiente valor *utility(board) = 420*.

![](2018-11-13-17-44-53.png)

## Algoritmo Minimax

El algoritmo de minimax implementado
en el programa es una versión en python
del siguiente pseudocódigo:

![](2018-11-13-19-31-39.png)


Las acciones son las columnas
donde pueden tirar los jugadores.
Y la prueba de que sea un nodo terminal
se hace verificando si alguien ya ganó
o si hay un empate y si se ha llegado
a la profundidad máxima en la que se desea
buscar.

## Algoritmo Alfa-Beta


El algoritmo poda alfa-beta implementado
en el programa es una versión en python
del siguiente pseudocódigo:

![](2018-11-13-19-45-07.png)

Las acciones son las columnas
donde pueden tirar los jugadores.
Y la prueba de que sea un nodo terminal
se hace verificando si alguien ya ganó
o si hay un empate y si se ha llegado
a la profundidad máxima en la que se desea
buscar.

## Instalación y ejecución

Instalación de módulos requeridos (`pygame`) para ejecutar el programa:

``pip install -r requisitos.txt``

Ejecución de la aplicación:

``python connect_this.py``

Dentro de la aplicación se puede seleccionar la dificultad del juego,
que está relacionada con la profundidad
máxima de búsqueda:

- Fácil - Profundidad 2
- Media - Profundidad 3
- Difícil - Profundidad 4

También es posible seleccionar
el algoritmo con el que tomará las 
decisiones la máquina:

- Minimax
- Poda alfa-beta

## Capturas del juego

![](2018-11-13-20-02-54.png)